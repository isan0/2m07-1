<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- BOOTSTRAP -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    
    <title>Document</title>
</head>
<body>
    <?php
    $productos = $_POST["productos"];
    $nombres = $_POST["nombres"];
    $precios = $_POST["precios"];
    $count = 0;
    ?>

    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="prod_list_01.php">Product List</a>
        </div>
    </nav>

    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Product Name</td>
                    <td>Price</td>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i=0; $i < $productos; $i++) {
                    $count++ ?>
                    <tr>
                        <?php if ($nombres[$i] == "") { ?>
                            <td colspan="3"> Product no insert </td> 
                        <?php }else { ?>
                            <td> <?= $count ?> </td>
                            <td> <?= $nombres[$i] ?> </td>
                            <td> <?= $precios[$i] ?> </td> 
                        <?php } ?>
                    </tr>
                    <?php            
                } ?>
            </tbody>
        </table>
    </div>
</body>
</html>