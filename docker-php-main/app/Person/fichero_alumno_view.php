<?php
include_once('_header.php');
include('Class/UploadClass.php');
include('Class/PersonClass.php');

$upload = new Upload("picture");

$alumno = new Person();
$alumno->setName($_POST['name']);
$alumno->setSurname($_POST['surname']);
$alumno->setAddress($_POST['address']);
$alumno->setComments($_POST['comments']);
$alumno->setPicture($upload->getPath());

?>
<div class="col-3 card">
    <img class="card-img-top" src="<?= $alumno->getPicture() ?>" alt="Card image cap">
    <div class="card-body">
        <h5 class="card-title"><?= $alumno->getName() . ' ' . $alumno->getSurname() ?></h5>
        <p class="card-text"><?= $alumno->getComments(); ?></p>
    </div>
    <div class="card-footer">
        <small class="text-muted"><?= $alumno->getAddress(); ?></small>
    </div>
</div>
<?php include_once('_footer.php') ?>