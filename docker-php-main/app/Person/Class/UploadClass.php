<?php
define("MAX", 5);
define("UPLOAD_FOLDER", "pictures/");

    class Upload{
      private $_fileName = '';
      private $_filePath = '';
      /*
      * Constructor: Inicia la subida del archivo
      * Entrada:
      * $name: Nombre del elemento del formulario del que gestionamos el $_FILE
      */
      function __construct($fileName){
        $this->_fileName = $fileName;
        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES[$this->_filename])) {
          $this->upload();
        }
      }
    
      /*
      * upload: Función que hace las operaciones necesarias para subir el archivo
      * al servidor
      */
      function upload(){
        try {
          if ($_FILES[$this->_filename]["error"] != 0) 
            throw new UploadError("Error: " . $_FILE[$this->_fileName]["error"]);
          
          $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
          $filename = $_FILES[$this->_fileName]["name"];
          $filetype = $_FILES[$this->_fileName]["type"];
          $filesize = $_FILES[$this->_fileName]["size"];

          $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!array_key_exists($ext, $allowed))
                throw new UploadError("Error: Please select a valid file format.");
          
          $maxsize = MAX * 1024 * 1024;
          if ($filesize > $maxsize)
              throw new UploadError("Error: File size is larger than the allowed limit.");

          if (!in_array($filetype, $allowed))
              throw new UploadError("Error: There was a problem uploading your file. Please try again.");

          move_uploaded_file($_FILES[$this->_fileName]["tmp_name"], UPLOAD_FOLDER . $filename);
          $this->_filePath = UPLOAD_FOLDER . $filename;
          
        }catch (UploadError $e) {
          echo $e->getMessage();
          die();
        } catch (Exception $e) {
          echo $e->getMessage();
          die();
        }
      }
    
      /*
      * Getters. Lo que quiere decir que los atributos de la clase son private
      */
      public function getPath(){
        return $this->_filePath;
      }
    
    
    }
      /*
      * OPCIONAL:
      * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
      * en la subida de archivos. Por ejemplo:
      * throw new UploadError("Error: Please select a valid file format.");
      */
      class UploadError extends Exception{}
?>