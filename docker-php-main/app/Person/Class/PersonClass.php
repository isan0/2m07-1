<?php
    class Person{
        private $_name;
        private $_surname;
        private $_address;
        private $_comments;
        private $_picture;

        /*
        Setters
        */
        public function setName($name){
            $this->_name=$name;
        }
        public function setSurname($surname){
            $this->_surname=$surname;
        }
        public function setAddress($address){
            $this->_address=$address;
        }
        public function setComments($comments){
            $this->_comments=$comments;
        }
        public function setPicture($picture){
            $this->_picture=$picture;
        }

        /*
        Getters
        */
        public function getName(){
            return $this->_name;
        }
        public function getSurname(){
            return $this->_surname;
        }
        public function getAddress(){
            return $this->_address;
        }
        public function getComments(){
            return $this->_comments;
        }
        public function getPicture(){
            return $this->_picture;
        }
    }

?>