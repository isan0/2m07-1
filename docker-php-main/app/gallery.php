<?php
include_once('_header.php');
include('Class/GalleryClass.php');
$gallery = new Gallery("fotos.txt");
?>

<div class="col-3 card">
    <?php
    foreach ($gallery->getGallery() as $value) {
        $rute = $value->getFileName();
        $title = $value->getTitle();
    } ?>
    
    <img class="card-img-top" src="<?= $rute ?>" alt="Card image cap">
    
    <div class="card-body">
        <p class="card-text"> <?= $title ?> </p>
    </div>
</div>
<?php include_once('_footer.php') ?>