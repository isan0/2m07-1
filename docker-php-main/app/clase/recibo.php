<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    <?php
    $num1 = $_POST["num1"];
    $num2 = $_POST["num2"];
    $operacion = $_POST["operacion"];
    ?>

    <div class="container-fluid">
        <h3>Resultado</h3>
        <?php
        if ($operacion == "sum") {
            $s = $num1 + $num2;
            echo "La suma de los numeros es " .$s;
        }
        if ($operacion == "rest") {
            $r = $num1 - $num2;
            echo "La resta de los numeros es " .$r;
        }
        if ($operacion == "mult") {
            $m = $num1 * $num2;
            echo "La multiplicacion de los numeros es " .$m;
        }
        if ($operacion == "div") {
            $d = $num1 / $num2;
            echo "La division de los numeros es " .$d;
        }
        if ($operacion == "exp") {
            $e = $num1 ** $num2;
            echo "El exponente del numero es " .$e;
        }
        ?>
    </div>
</body>
</html>