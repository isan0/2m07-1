<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    <?php
    $a = 4;
    $b = 3;
    $s = $a + $b;
    $r = $a - $b;
    $m = $a * $b;
    $d = $a / $b;
    $e = $a ** $b;
    ?>

    <div class="container-fluid">
        <h3>Sintaxis</h3>
        <table>
            <tr>
                <td>Operacion</td>
                <td>Valor</td>
            </tr>
            <tr>
                <td> A </td>
                <td> <?php echo $a ?> </td>
            </tr>
            <tr>
                <td> B </td>
                <td> <?php echo $b ?> </td>
            </tr>
            <tr>
                <td> A+B </td>
                <td> <?php echo $s ?> </td>
            </tr>
            <tr>
                <td> A-B </td>
                <td> <?php echo $r ?> </td>
            </tr>
            <tr>
                <td> A*B </td>
                <td> <?php echo $m ?> </td>
            </tr>
            <tr>
                <td> A/B </td>
                <td> <?php echo $d ?> </td>
            </tr>
            <tr>
                <td> AexpB </td>
                <td> <?php echo $e ?> </td>
            </tr>
        </table>
    </div>

    <div class="container-fluid">
        <h3>Operadores de Control</h3>
        <h4>Ej 1</h4>
        <?php
        $c;
        $loteria = array(61,32,43,61);
        foreach ($loteria as &$value) {
            if ($value == 61) {
                $c++;
            }
        }
        ?>
        <p>El numero 61 se repite <?php echo $c ?> </p>
    </div>

    <div class="container-fluid">
        <h4>Ej 2</h4>
        <div>
            <p>Ingresa el valor que quieres contar</p>
            <p>El array tiene los valores: </p>
            <?php
            foreach ($loteria as &$value) {
               echo $value. " ";
            }
            ?>
        </div>
        <form action="array.php" method="post">
            <div>
                Ingresa numero: <input type="text" name="valor">
            </div>
            <div>
            <input type="submit" value="Enviar">
            </div>
        </form>
    </div>

    <div class="container-fluid">
        <h3>Formulario 1</h3>
        <form action="recibo.php" method="post">
            Number 1:<input type="text" name="num1">
            <br>
            Number 2:<input type="text" name="num2">
            <br>
            Operacion:
            <select name="operacion">
                <option value="sum">+</option>
                <option value="rest">-</option>
                <option value="mult">*</option>
                <option value="div">/</option>
                <option value="exp">exp</option>
            </select>
            <br>
            <input type="submit" value="Enviar">
        </form>
    </div>
    
</body>
</html>