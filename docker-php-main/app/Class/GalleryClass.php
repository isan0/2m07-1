<?php
include('PictureClass.php');
class Gallery
{
    private $_gallery = [];
    private $_fileName;

  /*Constructor: Recibe la ruta del archivo fotos.txt*/
    function __construct($fileName){
      $this->_fileName=$fileName;
      $this->loadGallery();
    }

    /*
  *Recorre el archivo fotos.txt y para cada linea, crea un
  *elemento Picture que lo añade al atributo $_gallery[]
  */
    function loadGallery(){
      $file = fopen($this->_fileName, "r");

      while(!feof($file)){
        $linea = fgets($file);
        $separador = explode("###", $linea);
        $title = $separador[1];
        $rutaImg = $separador[0];

        $this->_gallery[] = new Picture($title,$rutaImg);
      }
      fclose($file);
    }

  /*
  *Getters.
  */
    public function getGallery(){
      return $this->_gallery;
    }
}
?>