<?php
class Picture
{
  private $_title;
  private $_fileName;

  /*Constructor*/
  function __construct($title, $fileName){
    $this->_fileName=$fileName;
    $this->_title=$title;
  }

  /*
  *Getters. Lo que quiere decir que los atributos de
  *title y filename son private
  */
  public function setTitle($title){
    $this->_title=$title;
  }

  public function setFileName($fileName){
    $this->_fileName=$fileName;
  }

  public function getTitle(){
    return $this->_title;
  }

  public function getFileName(){
    return $this->_fileName;
  }
}
?>