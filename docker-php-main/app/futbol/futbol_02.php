<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- BOOTSTRAP -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    
    <title>Document</title>
</head>
<body>
    <?php
    $jugadores = $_POST["jugadores"];
    $partidos = $_POST["partidos"];
    ?>
    <div class="container">
        <p> <?= "El total de jugadores son " .$jugadores ?> </p>
        <p> <?= "El total de partidos son " .$partidos ?> </p>
    </div>
    
    <div class="container">
        <form action="futbol_03.php" method="post">
            <div>
            <?php
            for ($i=1; $i <= $jugadores; $i++) { ?>
                Jugador: <?= $i ?>  <input type='text' name='nombres[]'>
                <?php for ($u=1; $u <= $partidos; $u++) { ?>
                    Partido <?= $u ?> <input type='text' name='goles[<?= $u ?>][]'>
                <?php }?>
                <br>
            <?php } ?>
            </div>
            <div>
                <input type="hidden" name="partidos" value="<?= $partidos ?>">
                <input type="hidden" name="jugadores" value="<?= $jugadores ?>">
            </div>
            <div>
                <input type="submit" value="Enviar">
            </div>
        </form>
    </div>

</body>
</html>

