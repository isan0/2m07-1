<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- BOOTSTRAP -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    
    <title>Document</title>
</head>
<body>
    <?php
    $jugadores = $_POST["jugadores"];
    $partidos = $_POST["partidos"];
    $goles = $_POST["goles"];
    $nombres = $_POST["nombres"];
    ?>

    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="futbol_01.php">Futbol</a>
        </div>
    </nav>

    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>Jugador</td>
                        <?php
                        for ($i=1; $i <= $partidos ; $i++) { ?>
                            <td>Partido <?= $i ?> </td>
                        <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($u=0; $u < $jugadores ; $u++) { ?>
                    <tr>
                        <td> <?= $nombres[$u] ?> </td>
                        <?php for ($e=1; $e <=$partidos ; $e++) { ?>
                            <td> <?= $goles[$e][$u] ?> </td>
                        
                        <?php }
                }
                ?>
                    </tr>
            </tbody>
        </table>
    </div>
</body>
</html>