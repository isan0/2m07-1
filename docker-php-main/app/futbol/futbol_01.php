<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- BOOTSTRAP -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    
    <title>Document</title>
</head>
<body>
    <div class="container">
        <h3>Formulario 2</h3>
        <form action="futbol_02.php" method="post">
            <div>
                Ingresa el total de jugadores: <input type="text" name="jugadores">
            </div>
            <div>
                Ingresa el total de partidos: <input type="text" name="partidos">
            </div>
            <div>
                <input type="submit" value="Enviar">
            </div>
        </form>
    </div>
</body>
</html>