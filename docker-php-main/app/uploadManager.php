<?php
define("RUTA", "pictures/");
include('Class/PictureClass.php');
uploadPicture('picture');

/*
* Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
* que almacena todas las fotos.
* Return: Devuelve la ruta final del archivo.
*/
function uploadPicture($name){
    $directory = "./pictures/";
    try {
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            if(isset($_FILES["picture"]) && $_FILES["picture"]["error"] == 0){
                $allowed = array("jpg"=>"image/jpg", "jpeg"=>"image/jpeg", "gif"=>"image/gif", "png"=>"image/png");
                $filename = $_FILES["picture"]["name"];
                $filetype = $_FILES["picture"]["type"];
                $filesize = $_FILES["picture"]["size"];

                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if(!array_key_exists($ext, $allowed)){
                    throw new UploadError("Error: El formato no es valido");
                }

                $maxsize = 5*1024*1024;
                if($filesize > $maxsize){
                    throw new UploadError("Error: Fichero demasiado grande");
                }


                if(in_array($filetype, $allowed)){
                    if(file_exists($directory . $filename)){
                        throw new UploadError($filename . " ya existe");
                        die();
                    }else{
                        $filename = $_FILES[$name]["name"];
                        $rutef = RUTA . $filename;
                        move_uploaded_file($_FILES[$name]["tmp_name"], $rutef);
                        $title = $_POST['title'];
                        addPictureToFile($rutef, $title);
                    }
                }else{
                    throw new UploadError("Error: No se a podido cargar la imagen");
                }
            }else{
                throw new UploadError("Error: No se a podido cargar la imagen");
            }
        }
    } catch (UploadError $e) {
        header('Location:index.php?upload=error&msg=' . urlencode($e->getMessage()));
    }
    header('Location:index.php?upload=success');;
    return $rutef;
}

/*
* Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
* fotografía recien subida
* Entradas:
*       $file_uploaded: La ruta del archivo
*       $title_uploaded: El titulo del archivo
* Return: null
*/
function addPictureToFile($file_uploaded,$title_uploaded){
    $file = fopen("fotos.txt", "a+");
    fputs($file, $file_uploaded . "###" . $title_uploaded . "\n");
    fclose($file);
}

/*
* Clase personalizada extendida de Exception que utilizaremos para lanzar errores
* en la subida de archivos. Por ejemplo:
* throw new UploadError("Error: Please select a valid file format.");
*/
class UploadError extends Exception{}
?>